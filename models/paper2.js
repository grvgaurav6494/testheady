var mongoose = require("mongoose");


var paperTestSchema = mongoose.Schema({
    paperType: String,
    dimension: [
    	{ type: mongoose.Schema.Types.ObjectId, ref: 'Dimension' }
    ]
});

module.exports = mongoose.model('TestPaper', paperTestSchema);





